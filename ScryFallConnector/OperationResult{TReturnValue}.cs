﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScryFallConnector
{
    public sealed class OperationResult<TReturnValue> : IOperationResult<TReturnValue>
    {
        public OperationResult(TReturnValue returnValue, IReadOnlyList<ValidationIssue> validationIssues, OperationOutcome outcome)
        {
            this.ReturnValue = returnValue;
            this.Outcome = outcome;
            this.ValidationIssues = validationIssues;
        }

        public OperationResult(TReturnValue returnValue, OperationOutcome outcome)
            : this(returnValue, Array.Empty<ValidationIssue>(), outcome)
        {
        }

        public IReadOnlyList<ValidationIssue> ValidationIssues { get; }

        IReadOnlyList<ValidationIssue> IOperationResult<TReturnValue>.Notifications => this.ValidationIssues;

        public TReturnValue ReturnValue { get; }

        public OperationOutcome Outcome { get; }
    }
}
