﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScryFallConnector
{
    public interface IOperationResult<out TReturnValue>
    {
        IReadOnlyList<ValidationIssue> Notifications { get; }

        TReturnValue ReturnValue { get; }
    }
}
