﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ScryFallConnector.Api
{
    public class ConnectorBase
    {
        private const string BaseUrl = "api.scryfall.com";

        protected HttpClient httpClient;

        public ConnectorBase()
        {
            httpClient = new HttpClient
            {
                BaseAddress = new Uri(BaseUrl)
            };
        }
    }
}
